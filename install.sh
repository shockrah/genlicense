#!/bin/sh

opt=`echo $1 | tr -d '-'`
case $opt in
	g|global) 
		rm -f /usr/local/bin/genlicense
		cp genlicense /usr/local/bin/genlicense
		mkdir -p $HOME/.config/genlicense
		cp templates/* $HOME/.config/genlicense
		;;
	l|local) 
		rm -f $HOME/.local/bin/genlicense
		cp genlicense $HOME/.local/bin/genlicense
		mkdir -p $HOME/.config/genlicense
		cp templates/* $HOME/.config/genlicense
		;;
	*) 
cat <<EOF
Options:
   g|global : copy build.sh to /usr/bin/genlicense
   l|local  : copy buils.dh to $HOME/.local/bin/genlicense
EOF
;;
esac

